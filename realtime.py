#!/usr/bin/env python3
# coding: utf-8

import re
import os
import time
from time import sleep
from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
import hyperloglog

# Preparing SparkContext
from hdfs import Config
import subprocess

client = Config().get_client()
nn_address = subprocess.check_output('hdfs getconf -confKey dfs.namenode.http-address', shell=True).strip().decode("utf-8")

sc = SparkContext(master='yarn-client')

# Preparing base RDD with the input data
DATA_PATH = "/data/realtime/uids"

batches = [sc.textFile(os.path.join(*[nn_address, DATA_PATH, path])) for path in client.list(DATA_PATH)]

# Creating QueueStream to emulate realtime data generating
BATCH_TIMEOUT = 2 # Timeout between batch generation
ssc = StreamingContext(sc, BATCH_TIMEOUT)
dstream = ssc.queueStream(rdds=batches)

seg_iphone, seg_firefox, seg_windows = [],[],[]#hyperloglog.HyperLogLog(0.01), hyperloglog.HyperLogLog(0.01), hyperloglog.HyperLogLog(0.01)
finished = False
printed = False

def set_ending_flag(rdd):
    global finished
    if rdd.isEmpty():
        finished = True

dstream.foreachRDD(set_ending_flag)


def p(rdd):
    global seg_window, seg_firefox, seg_iphone , finished, printed
    for log in rdd.collect():
        log = log.lower().split('\t')
        if 'iphone' in log[1]:
            seg_iphone.append(log[0])
        if 'firefox' in log[1]:
            seg_firefox.append(log[0])
        if 'windows' in log[1]:
            seg_windows.append(log[0])
    if finished:
        a = [['seg_iphone',len(set(seg_iphone))], ['seg_firefox',len(set(seg_firefox))], ['seg_windows',len(set(seg_windows))]]
	a = sorted(a, key=lambda point: point[1], reverse=True)
	for i in a:
            print("{}\t{}".format(i[0],i[1]))

dstream.foreachRDD(p)

ssc.checkpoint('./checkpoint{}'.format(time.strftime("%Y_%m_%d_%H_%M_%s", time.gmtime())))  # checkpoint for storing current state
ssc.start()
while not finished:
     time.sleep(0.1)
ssc.stop()
sc.stop()
